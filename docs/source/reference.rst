The Library reference
=====================

This is the reference for the entire library

The library does many things, neatly separated into packages for convenience,
here's a list of what it can do:

.. toctree::
   :maxdepth: 2

   funcs/regressions
   funcs/classification
   funcs/clustering
   funcs/preprocessing
   funcs/data
   funcs/utils
   funcs/misc
   funcs/fcnn

