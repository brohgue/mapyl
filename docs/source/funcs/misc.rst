=============
Miscellaneous
=============

Saving Models
^^^^^^^^^^^^^

Since the models are all built using OOP patterns, it is very easy to save them to files.
For example:

    .. code-block:: python

        >>> import pickle 
        >>> X = np.array([[4, -3], 
                        [9, -6],
                        [3, -5],
                        [4, -3],
                        [9, -5,]])
        >>> km = KMeans(K=2, tol=0.001)
        >>> km.fit(X, iters=100)
        >>> with open(f'a_file.pickle', 'wb') as file: pickle.dump(km, file)

This uses the standard python library  ``pickle`` to save the class to a binary file. 
To load it use ``pickle.load(a_file.pickle)``