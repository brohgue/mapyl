==========
Clustering
==========

Module for clustering data.
These algorithms do not require labeled data. They only need the input data 
(and the number of clusters in the case of KMeans). They ONLY work with 
data that is clustered together, hence the name of the module.

.. currentmodule:: mapyl.clustering

.. autoclass:: KMeans
    :members:

Usage:

.. code-block:: python

    >>> X = np.array([[4, -3], 
                        [9, -6],
                        [3, -5],
                        [4, -3],
                        [9, -5,]])
    >>> km = KMeans(K=2, tol=0.001)
    >>> km.fit(X, iters=100)
    >>> print(km.predict(np.array([[3, -2]])))
    >>> 0

.. autoclass:: DBSCAN
    :members:

In order for the DBSCAN instance to predict values, the core sample
indices and labels can be used to train another algorithm, such as KNN:

.. code-block:: python

    >>> X = np.array([[4, -3], 
                        [9, -6],
                        [3, -5],
                        [4, -3],
                        [9, -5,]])
    >>> db = DBSCAN(eps=2, minpoints=5)
    >>> db.fit(X)
    >>> knn = KNearestNeighbors(K=5)
    >>> knn.fit(X, db.labels)
    >>> print(knn.predict(np.array([3, -2])))
    >>> [0]