=========
Utilities
=========

Module containing utilities which are used in the library

.. currentmodule:: mapyl.utils

.. autoclass:: PolyExp
    :members:

.. autoclass:: Mode
    :members:

.. autoclass:: Accuracy
    :members: