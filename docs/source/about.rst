About
=====

Why This Library?
-----------------

MaPyL was created with a focus on simplicity and usability, 
we saw that the current libraries for machine learning are way too complicated
and require too much before-hand knowledge to be used, so we created MaPyL.

If you didnt catch it, 'MaPyL' stands for Machine learning for Python Library
