.. MaPyL documentation master file, created by
   sphinx-quickstart on Wed Feb 16 08:50:20 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the MaPyL docs!
==================================

A Python Machine learning library dedicated to simplicity and usability

.. toctree::
   :maxdepth: 2
   :caption: Contents:

To install simply do:

``pip install mapyl``

be sure you have numpy installed, as the library heavily depends on that

Beware of the python version you are using, the library is built on python 3.7,
python 2.x is not compatible and other versions (>3.9) are not guaranteed to work

The Features of MaPy:
=======================
- Regression
- Support Vector Machines
- Clustering
- Data preprocessing
- Data generation
- Fully Connected Neural Networks


General
==================

An about section and a reference for usage

.. toctree::
   :maxdepth: 2

   reference
   about
   

Contributing
============

Contributing is currently not open, the library is still in the very early stages of development
so we concluded that contributions from outside the team would be more trouble than help.
