from ._2d_data import GenData
from ._2d_data import SplitData
from ._acc import accuracy
