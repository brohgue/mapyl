from ._regression import LinearRegressor
from ._regression import PolyRegressor
from ._graddesc import GradientDescent
from ._graddesc import SGD
from .logistic_regression import BinLogitRegressor