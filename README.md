# MaPyL

### A Python Machine learning library

A python machine learning library dedicated to simplicity and usability

MaPyL has the purpose of introducing a simple and functional machine learning library for those who don't want to spend days reading documentations.

All it needs is NumPy!

### Read The Docs!

Our documentation is available [here](https://mapyl.readthedocs.io)
We promise it's easy for you to get around.

## Features

The current library has the following features:

- Linear, polynomial and logistic regression
- Classification
- Feature scaling
- Support Vector Machines
- Clustering (K-means and DBSCAN)

## Contributing

Currently contributing is closed and we will not accept any contributions outside of our team.

This is because the library is still in the very (very) early stages of development and there is still much work to be done, and random people working on the library at the same time as us won't help much right now, so hold your horses while we work on it. 

Since the first verion was already released, we are considering opening the library for more people, although we don't have a due date yet.


